; Recruiter drush make build file for drupal.org packaging.
core = 7.x

api = 2

; Modules

projects[acl][version] = "1.1"

projects[addressfield][version] = 1.3

projects[admin_menu][version] = 3.0-rc6

projects[autocomplete_deluxe][version] = 2.3

projects[better_formats][version] = 1.0-beta2

projects[ctools][version] = 1.14
; Term URL transformation fails when term includes a dash https://www.drupal.org/node/2320779#comment-11059807
projects[ctools][patch][] = https://www.drupal.org/files/issues/ctools-term-dashes-2320779-7.patch

projects[colorbox][version] = 2.13

projects[content_access][version] = "1.2-beta2"
; Node Access bug if core patch is applied https://www.drupal.org/node/1097248#comment-6613538
projects[content_access][patch][] = "https://www.drupal.org/files/1097248-content-access-node-grants-24.patch"

projects[context_admin][version] = "1.2"
; PHP warning https://www.drupal.org/project/context_admin/issues/3021789
projects[context_admin][patch][] = https://www.drupal.org/files/issues/2018-12-20/ca-3021789-2.patch

projects[content_taxonomy][version] = 1.0-rc1

projects[context][version] = 3.10

; Update to 2.11-beta3 or 2.11 once that gets released.
projects[date][version] = 2.x-dev

projects[diff][version] = 3.4

projects[email][version] = "1.3"

projects[entity][version] = 1.9

projects[entityreference][version] = 1.5

projects[facetapi][version] = 1.5

projects[facetapi_pretty_paths][version] = "1.0"

projects[features][version] = "1.0"
; Fixes import of menu links of disabled modules (https://www.drupal.org/node/1241108)
projects[features][patch][] = "https://www.drupal.org/files/1241108-d7-1.patch"
; Remove mtime from .info export (added by Drupal 7.33) https://www.drupal.org/node/2381739
projects[features][patch][] = https://www.drupal.org/files/issues/features-mtime-1x-2381739-14.patch

projects[field_collection][version] = 1.0-beta13
; EntityFieldQueryException: Unknown field when deleting fields https://www.drupal.org/node/1866032
projects[field_collection][patch][] = "https://www.drupal.org/files/fieldcol-1866032-7.patch"
; Notice: Undefined index: revision_id in field_collection_field_get_entity() https://www.drupal.org/node/1822844
projects[field_collection][patch][] = https://www.drupal.org/files/issues/notice_undefined-1822844-39.patch
; Replace confusing empty 'Too many items.' page with Access Denied page https://www.drupal.org/node/2841004
projects[field_collection][patch][] = https://www.drupal.org/files/issues/fc-too-many-items-2841004-4.patch
projects[field_permissions][version] = "1.0"

projects[flag][version] = 3.9

projects[i18n][version] = 1.18
; Fatal error: Cannot use string offset as an array in ../sites/all/modules/i18n/i18n_field/i18n_field.inc on line 165
projects[i18n][patch][] = https://www.drupal.org/files/issues/i18n-1805066-5.patch

projects[link][version] = 1.6
; Change external links as little as possible https://www.drupal.org/node/1914072#comment-11618049
projects[link][patch][] = https://www.drupal.org/files/issues/link_module_displays-1914072-34.patch
; mailto: link with subject parameter and upper case letter does not validate https://www.drupal.org/node/2813057
projects[link][patch][] = https://www.drupal.org/files/issues/link-mailto-2813057-7.patch

projects[message][version] = 1.12
; Message translations https://www.drupal.org/node/1479026
projects[message][patch][] = https://www.drupal.org/files/message-i18n-1479026-9.patch

projects[migrate][version] = 2.11

projects[migrate_extras][version] = "2.5"

projects[panels][version] = 3.9

projects[pathauto][version] = 1.3

projects[pm_existing_pages][version] = "1.4"

projects[profile2][version] = "1.3"
; Add ctools relationship
projects[profile2][patch][] = "https://www.drupal.org/files/1011370-profile2-ctools.patch"

projects[recruiter_features][version] = 1.3

projects[roleassign][version] = 1.1

projects[role_export][version] = "1.0"

projects[rules][version] = 2.11
; Add a Rules Condition: entity has required fields set https://drupal.org/node/2111559
projects[rules][patch][] = https://drupal.org/files/issues/rules-entity-required-fields-2111559-3.patch

projects[rules_autotag][version] = 1.4

projects[rules_link][version] = "1.1"
; Redirect instead of access denied on confirm form https://www.drupal.org/project/rules_link/issues/3019303
projects[rules_link][patch][] = https://www.drupal.org/files/issues/2018-12-12/rules_link-access-denied-confirm-form-redirect-3019303-5.patch

projects[search_api][version] = 1.26

projects[search_api_saved_searches][version] = "1.7"
; Access key isn't always generated for saved searches https://www.drupal.org/node/2717101
projects[search_api_saved_searches][patch][] = https://www.drupal.org/files/issues/access_key_generation-2717101-1.patch
; Store saved search object into form_state and enable custom submit handlers to use it. https://www.drupal.org/project/search_api_saved_searches/issues/3036088
projects[search_api_saved_searches][patch][] = https://www.drupal.org/files/issues/2019-02-27/search_api_saved_searches-add-search-to-form_state.patch
; Fix quantity token https://www.drupal.org/project/search_api_saved_searches/issues/3038333
projects[search_api_saved_searches][patch][] = https://www.drupal.org/files/issues/2019-03-07/fix-result-count-3038333-2-7.patch
; Result count is doubled https://www.drupal.org/project/search_api_saved_searches/issues/3042087
projects[search_api_saved_searches][patch][] = https://www.drupal.org/files/issues/2019-03-21/search-api-saved-searches-fix-results-count-3042087-2_0.patch

projects[search_api_solr][version] = 1.14

projects[strongarm][version] = "2.0"

projects[synonyms][version] = "1.1"

projects[taxonomy_csv][version] = "5.10"

projects[taxonomy_formatter][version] = "1.4"
; Fix empty labels https://www.drupal.org/node/1352050#comment-7028512
projects[taxonomy_formatter][patch][] = "https://www.drupal.org/files/taxonomy_formatter-label-on-empty-1352050-9.patch"

projects[taxonomy_manager][version] = "1.0"

projects[term_level][version] = "1.x-dev"
projects[term_level][patch][] = "https://www.drupal.org/files/issues/term_level-views_relation-2513768-3.patch"

projects[token][version] = 1.7

projects[variable][version] = "2.5"
; Have a separate permission to configure variable realms (e.g. variable
; translations);
projects[variable][patch][] = "https://www.drupal.org/files/issues/variable-admin-permissions-2268415-23.patch"

projects[views][version] = 3.23
; Remove filter_xss_admin() from result area because we wan't to insert all html tags there
projects[views][patch][] = https://www.drupal.org/files/issues/2019-08-12/remove-filter_xss_admin-from-views_handler_area_result-3074270-2.patch

projects[views_bulk_operations][version] = 3.5
; Fix "Select All does not process all items" - https://www.drupal.org/project/views_bulk_operations/issues/2646242
projects[views_bulk_operations][patch][] = https://www.drupal.org/files/issues/2019-03-14/process-all-items-2646242-45.patch

projects[wysiwyg][version] = 2.4

projects[wysiwyg_linebreaks][version] = 1.8

;  -  Libraries  -

; Also add the colorbox library.
libraries[colorbox][download][url] = https://github.com/jackmoore/colorbox/archive/1.6.4.tar.gz
libraries[colorbox][download][type] = "get"

; CKEditor
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "https://download.cksource.com/CKEditor/CKEditor/CKEditor%204.11.4/ckeditor_4.11.4_full.zip"


;  - Recruiter Cloudy base theme -

projects[block_class][version] = 2.3
; Features support https://www.drupal.org/node/1230234
projects[block_class][patch][] = https://www.drupal.org/files/issues/block_class-features-support-1230234-42.patch

projects[omega][type] = theme
projects[omega][version] = "4.4"
;Managed File on form settings https://www.drupal.org/node/2186031
projects[omega][patch][] = "https://www.drupal.org/files/issues/omega-form-state-file-references-2186031-8.patch"
;Do not blow out the scripts data inside omega_element_info_alter https://www.drupal.org/node/2492461
projects[omega][patch][] = "https://www.drupal.org/files/issues/omega-2492461-1-smarter-element-info-alter.patch"
;Modules are listed as missing themes in Drupal 7.50, triggers user warnings https://www.drupal.org/node/2762793
projects[omega][patch][] = "https://www.drupal.org/files/issues/omega-modules-listed-as-missing-themes-2762793-13.patch"
;Add delta specific theme hook suggestion for menu blocks after block__nav https://www.drupal.org/node/2279155
projects[omega][patch][] = "https://www.drupal.org/files/issues/block__nav_suggestions-2279155-7.patch"
;Remove redundant aria role for articles https://www.drupal.org/project/omega/issues/2928873
projects[omega][patch][] = "https://www.drupal.org/files/issues/remove_aria_role_articles-2928873-2.patch"

projects[cloudy][type] = theme
projects[cloudy][version] = 1.5

projects[libraries][version] = 2.5
